import { DestinoViaje } from './destino-viaje.model';
import {Injectable} from '@angular/core';
import { Store } from '@ngrx/store';
import {
		DestinosViajesState,
		NuevoDestinoAction,
		ElegidoFavoritoAction
	} from './destino-viaje-state.model';
import {AppState} from './../app.module';

@Injectable()
export class DestinoApiClient {
	destinos:DestinoViaje[]=[];
	constructor(private store: Store<AppState>) {

	   
	   this.store
			.select(state => state.destinos)
			.subscribe((data) => {
				console.log("destinos sub store");
				console.log(data);
				this.destinos = data.items;
		});

		this.store
			.subscribe((data) => {
				console.log("all store");
				console.log(data);
			});
	}
	add(d:DestinoViaje){
	  this.store.dispatch(new NuevoDestinoAction(d));
	}

	
	getAll():DestinoViaje[]{
		return this.destinos;
	}

	getById(id:String):DestinoViaje{
		return this.destinos.filter(function(d){return d.id.toString() == id;})[0];
	}

	elegir(d: DestinoViaje){
		this.store.dispatch(new ElegidoFavoritoAction(d));
	}

} 