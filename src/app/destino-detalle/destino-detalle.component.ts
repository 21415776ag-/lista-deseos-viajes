import { Component, OnInit } from '@angular/core';
import { DestinoApiClient } from './../models/destino-api-client';
import { ActivatedRoute } from '@angular/router';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css']
})
export class DestinoDetalleComponent implements OnInit {
  destino:DestinoViaje;
  constructor(private route: ActivatedRoute, private destinosApiClient:DestinoApiClient) { }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}
