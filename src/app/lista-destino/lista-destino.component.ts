import { DestinoViaje } from './../models/destino-viaje.model';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoApiClient } from './../models/destino-api-client';
import { DestinosViajesState, ElegidoFavoritoAction } from './../models/destino-viaje-state.model';
import { Store } from '@ngrx/store';
import {AppState} from './../app.module'

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css']
})
export class ListaDestinoComponent implements OnInit {


  @Output() onItemAdded: EventEmitter<DestinoViaje>;
 
  updates:string[];

  constructor(public destinoApiClient:DestinoApiClient ,
    
    private store: Store<AppState>
    ) { 

    this.updates = [];

  }

  ngOnInit(): void {

    /*this.store.select(state => state.destinos)
      .subscribe(data => {
        let d = data.favorito;
        if (d != null) {
          this.updates.push("Se eligió: " + d.nombre);
        }
      });*/

      this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const f = data;
        if (f != null) {
          this.updates.push('Se eligió: ' + f.nombre);
        }
      });
  }


  agregado(d: DestinoViaje){
    this.destinoApiClient.add(d);
    this.store.dispatch(new ElegidoFavoritoAction(d));

  }



  elegido(e:DestinoViaje){
    this.destinoApiClient.elegir(e);
    this.store.dispatch(new ElegidoFavoritoAction(e));
  }

}
